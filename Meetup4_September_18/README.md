# Meetup 4
In this meetup we'll use show how to use all the Machine Learning power of H2O within a Spark data pipeline.

In this repository you can find the slides presented at the Meetup and a reference to the Notebook used to run the demo.

The demo application built during the Meetup was adapted from an existing Sparkling Water tutorial by H2O and shows how to detect spam messages within a dataset containing sample SMS messages.

The demo was presented by Bruno Kemmer, data engineer @Semantix and is available here:

https://github.com/bkemmer/h2O-tutorials-adapted


the-ai-academy

