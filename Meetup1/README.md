# Meetup 1
In this meetup we'll use H2O AutoML to address the challenge launched by Porto Seguro on Kaggle.

In this repository we are sharing the datasets and H2O flow code used in the demo at H2O Meetup #1 in São Paulo.
This classigication use case is taken from the Kaggle “Porto Seguro’s Safe Driver Prediction” competition which can be found here:

https://www.kaggle.com/c/porto-seguro-safe-driver-prediction

and has the objetive to predict which Porto Seguro’s insurance holders will file a claim. 

In the H2O Flow we will:

- import the training dataset
- perform an Exploratory Data Analysis (EDA)
- perform some quick Data Preparation. 
- use H2O AutoML funcionalities to build a model
- create the prediction using the test dataset provided in the competition

the-ai-academy

